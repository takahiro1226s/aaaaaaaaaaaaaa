$(function() {  //gifの表示位置の調整と「ローディング後に表示される要素」を非表示にする。
    var loader_h = $('#loader').height();
    $('.wrapper').css('display','none');
    $('#loader-bg ,#loader').css('display','block');
    $('#loader').css({
      'height' : loader_h + 'px',
      'margin-top' : '-' + loader_h + 'px'
    });
  });


  $('#loader p').delay(1000).fadeIn(3000);


  $(window).load(function () {  //全ての読み込みが完了したら「ローディング後に表示される要素」を表示する。
    $('#loader-bg').delay(900).fadeOut(1000);
    $('#loader').delay(600).fadeOut(1000);
    $('.wrapper').css('display', 'block');
  });
   
  //10秒たったら強制的にロード画面を非表示
  $(function(){
    setTimeout('stopload()',4000);
  });
               
  function stopload(){
    $('.wrapper').css('display','block');
    $('#loader-bg').delay(600).fadeOut(2000);
    $('#loader').delay(600).fadeOut(2000);
  }


  $(function(){
      $('a[href^=#]').click(function(){
          var speed = 900;
          var href= $(this).attr("href");
          var target = $(href == "#" || href == "" ? 'html' : href);
          var position = target.offset().top;
          $("html, body").animate({scrollTop:position}, speed, "swing");
          return false;
      });
  });


  $(window).fadeThis();

  var menuHeight = $("#menu-wrap").height();
  var startPos = 0;
  $(window).scroll(function(){
    var currentPos = $(this).scrollTop();
    if (currentPos > startPos) {
        if($(window).scrollTop() >= 200) {
        $("#menu-wrap").css("top", "-" + menuHeight + "px");
          }
    } else {
      $("#menu-wrap").css("top", 0 + "px");
    }
    startPos = currentPos;
  });
  
  $(function(){
    $('.effect div, .effect i').css("opacity","0");
    $(window).scroll(function (){
      $(".effect").each(function(){
        var imgPos = $(this).offset().top;    
        var scroll = $(window).scrollTop();
        var windowHeight = $(window).height();
        if (scroll > imgPos + windowHeight/5){
          $("i, div",this).css("opacity","1" );
          $("i",this).css({ 
            "font-size": "100px",
            "padding": "0 20px 40px"
          });
        } else {
          $("i, div",this).css("opacity","0" );
          $("i",this).css({ 
            "font-size": "20px",
            "padding": "20px"
          });
        }
      });
    });
  });
  
  $(function(){
    $(window).scroll(function (){
      var scroll = $(window).scrollTop();
      var windowHeight = $(window).height();
      if(scroll >= windowHeight/5) {
        $('#centerBoxFirst').fadeOut();
      } else {
        $('#centerBoxFirst').fadeIn();
      }
    });
  });

  $(document).ready(function() {
    var pagetop = $("#page-top");
    pagetop.click(function () {
      $('body, html').animate({ scrollTop: 0 }, 2000, "swing");
      return false;
    });
  });


  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-775394-6', 'auto');
  ga('send', 'pageview');



